from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)

    bin = models.ForeignKey(
        BinVO, related_name="bin", on_delete=models.CASCADE, null=True
    )

    def get_api_url(self):
        return reverse("api_shoes", kwargs={"id": self.id})

    def __str__(self):
        return {self.model_name}

    class Meta:
        ordering = ("manufacturer", "model_name", "color", "bin")
