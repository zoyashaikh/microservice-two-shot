from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Shoes, BinVO


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
    ]


class ShoesEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "bin",
    ]

    encoders = {
        "bin": BinVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoe = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoe},
            encoder=ShoesEncoder,
        )
    else:
        content = json.loads(request.body)
    try:
        bin_href = content["bin"]
        bin = BinVO.objects.get(import_href=bin_href)
        content["bin"] = bin
    except BinVO.DoesNotExist:
        return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

    shoe = Shoes.objects.create(**content)
    return JsonResponse(
        shoe,
        encoder=ShoesEncoder,
        safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_shoe(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoesEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            shoe = Shoes.objects.get(id=pk)
            shoe.delete()
            return JsonResponse(
                shoe,
                encoder=ShoesEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
