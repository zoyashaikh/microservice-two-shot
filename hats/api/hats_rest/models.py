from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100)

    def __str__(self):
        return self.closet_name


class Hats(models.Model):
    material = models.CharField(max_length=100)
    style = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField(null=True, blank=True)

    location = models.ForeignKey(
        LocationVO, related_name="hats", on_delete=models.CASCADE, null=True
    )

    def get_api_url(self, request=None):
        return reverse("api_hats_detail", kwargs={"pk": self.pk})

    def __str__(self):
        return self.material

    class Meta:
        ordering = ["material"]
