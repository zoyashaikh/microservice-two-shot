from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hats, LocationVO


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
    ]


class HatsEncoder(ModelEncoder):
    model = Hats
    properties = [
        "material",
        "style",
        "color",
        "picture",
        "href",
        "location",
    ]

    encoders = {
        "location": LocationVOEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_hats(request):
    if request.method == "GET":
        hat = Hats.objects.all()
        return JsonResponse(
            {"hats": hat},
            encoder=HatsEncoder,
        )
    else:
        content = json.loads(request.body)
    try:
        hat_href = content["location"]
        location = LocationVO.objects.get(import_href=hat_href)
        content["location"] = location
    except LocationVO.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid location id"},
            status=400
        )

    hat = Hats.objects.create(**content)
    return JsonResponse(
        hat,
        encoder=HatsEncoder,
        safe=False,
        )


@require_http_methods(("GET", "DELETE"))
def api_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id-id)
            return JsonResponse(
                hat,
                encoder=HatsEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = Hats.objects.get(id-id)
            hat.deletel()
            return JsonResponse(
                hat,
                encoder=HatsEncoder,
                safe=False
            )
        except Hats.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
